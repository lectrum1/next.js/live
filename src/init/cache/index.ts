// Core
import { InMemoryCache } from '@apollo/client';

const cache = new InMemoryCache({
    typePolicies: {
        Courses: {
            keyFields: [
                'badge',
                'created',
                'createdBy',
                'description',
                'duration',
                'hash',
                'info',
                'poster',
                'price',
                'rating',
                'technologies',
                'views',
                'votes',
            ],
        },
    },
});

export { cache };
