// Core
import { createContext, ReactElement, ReactNode } from 'react';
import { applySnapshot } from 'mobx-state-tree';

// types
import { IStore } from 'types';

// Others
import { RootStore } from 'models';

interface IStoreProvider {
    children: ReactElement | ReactNode,
    dataStore: IStore,
}

let store: IStore | null = null;

export function initializeStore(snapshot: IStore | null = null): IStore {
    const _store = store ?? RootStore.create({ courses: []});

    if (snapshot) {
        applySnapshot(_store, snapshot);
    }

    if (typeof window === 'undefined') {
        return _store;
    }

    if (!store) {
        store = _store;
    }

    return store;
}

export const StoreContext = createContext<IStore>(undefined!);

export const StoreProvider = ({ children, dataStore }: IStoreProvider) => {
    return (
        <StoreContext.Provider value = { dataStore }>
            {children}
        </StoreContext.Provider>
    );
};

export const getStore = (): IStore | null => store;

