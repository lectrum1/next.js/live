// Core
import { ApolloClient, ApolloLink } from '@apollo/client';
import apolloLogger from 'apollo-link-logger';

// Type
import { ICacheShape } from 'types/cache';

// Instruments
import { verification } from 'services';
import { setHttpLink, setAuthLink, errorLink, retryLink } from './link';
import { cache } from './cache';
import { ContextInto } from 'types';

let apolloClient: ApolloClient<ICacheShape>;

const { isBrowser, environment } = verification;
const { isDevelopment } = environment();

function createApolloClient(context?: ContextInto): ApolloClient<ICacheShape> {
    const httpLink = setHttpLink({
        uri: process.env.NEXT_PUBLIC_EXTERNAL_GRAPH_URL,
    });
    const authLink = setAuthLink(context);

    let link;

    if (!isBrowser() && context?.req) {
        link = ApolloLink.from([
            // context links
            authLink,
            // errors
            errorLink,
            // http
            httpLink,
        ]);
    } else {
        const links = [
            // context links
            authLink,
            // errors
            errorLink,
            retryLink,
            // http
            httpLink,
        ];

        if (isBrowser() && isDevelopment) {
            links.unshift(apolloLogger);
        }

        link = ApolloLink.from(links);
    }

    return new ApolloClient({
        ssrMode: !isBrowser(),
        link,
        cache,
    });
}

export const initializeApollo = (
    initialState: {},
    context?: ContextInto,
) => {
    const readyApolloClient = apolloClient || createApolloClient(context);

    // If your page has Next.js data fetching methods that use Apollo Client, the initial state
    // get hydrated here
    if (initialState) {
        readyApolloClient.cache.restore(initialState);
    }

    // For SSG and SSR always create a new Apollo Client
    if (!isBrowser()) {
        return readyApolloClient;
    }

    // Create the Apollo Client once in the client
    if (!apolloClient) {
        apolloClient = readyApolloClient;
    }

    return readyApolloClient;
};
