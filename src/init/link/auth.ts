// Core
import { setContext } from '@apollo/client/link/context';
import nookies from 'nookies';

// Types
import { ContextInto } from 'types';

export const setAuthLink = (context?: ContextInto) => setContext((_, { headers }) => {
    const cookies = nookies.get(context);

    // get the authentication token from local storage if it exists
    const token = cookies?.token;

    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : '',
        },
    };
});
