// Core
import { RetryLink } from '@apollo/client/link/retry';

export const retryLink = new RetryLink({
    delay: {
        initial: 300,
    },
    attempts: {
        max:     3,
        // eslint-disable-next-line
        retryIf: (error, _operation) => !!error,
    },
});
