// Type
import type { HttpOptions } from '@apollo/client';

// Core
import { createHttpLink } from '@apollo/client';
import fetch from 'isomorphic-unfetch';

export const setHttpLink = (options?: Partial<HttpOptions>) => createHttpLink({
    fetch,
    credentials: 'include',
    ...options,
});
