// Core
import { onError } from '@apollo/client/link/error';

// Logger
import { developmentLogger, productionLogger } from 'services/loggers';

// Instruments
import { destroySession } from 'lib/hook';

export const errorLink = onError(({
    graphQLErrors,
    networkError,
    operation,
    forward,
}) => {
    if (graphQLErrors) {
        for (let error of graphQLErrors) {
            switch (error.extensions?.code) {
                case 'UNAUTHENTICATED': {
                    // error code is set to UNAUTHENTICATED
                    // when AuthenticationError thrown in resolver

                    // modify the operation context with a new token
                    const oldHeaders = operation.getContext().headers;
                    operation.setContext({
                        headers: {
                            ...oldHeaders,
                            // authorization: getNewToken(),
                        },
                    });

                    // retry the request, returning the new observable
                    return forward(operation);
                }

                case 'UNAUTHORIZED': {
                    return  destroySession();
                }

                default: {
                    graphQLErrors.forEach(({
                        message,
                        locations,
                        path,
                    }) => {
                        const fullMessage = `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`;
                        developmentLogger.error('Error', fullMessage);
                        productionLogger.error('Error', fullMessage);
                    });
                }
            }
        }
    }

    if (networkError) {
        developmentLogger.error(`[Network error]: ${networkError}`);
    }
});
