// Types
import { IExecutor, $QueryOptions } from 'types/gql';

// Other
import { GetServerSidePropsContext } from 'next';

// Instruments
import { initializeApollo } from 'init/apollo';
import { verification } from './verification';
import { developmentLogger, gqlLogger } from './loggers';

const { environment } = verification;

export const executeQuery = async (
    context: GetServerSidePropsContext,
    executor: IExecutor<any>,
) => {
    const { isDevelopment, isProduction } = environment();
    let savedQuery: $QueryOptions | null = null;

    try {
        const apolloClient = initializeApollo({}, context);

        const userAgent = context?.req?.headers[ 'user-agent' ];

        const execute = async (query: $QueryOptions) => {
            savedQuery = query;

            if (isDevelopment) {
                gqlLogger.info(query, {
                    isStarted: true,
                });
            }

            try {
                return await apolloClient.query({
                    ...query,
                    context: {
                        headers: {
                            'user-agent': userAgent,
                        },
                    },
                });
            } catch (error) {
                if (isProduction) {
                    gqlLogger.error(query, error, context);
                }

                return undefined;
            } finally {
                if (isDevelopment) {
                    gqlLogger.info(query, {
                        isFinished: true,
                    });
                }
            }
        };

        await executor(execute);

        return apolloClient.cache.extract();
    } catch (error) {
        if (isProduction && savedQuery) {
            gqlLogger.error(savedQuery, error, context);
        } else {
            developmentLogger.error('Error', String(error));
        }

        return {};
    }
};
