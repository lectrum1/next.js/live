// Core
import winston from 'winston';

// Instruments
import { verification } from 'services/verification';
import BrowserConsole from 'lib/winstonTransportBrowserConsole';

const { isBrowser } = verification;

export const developmentLogger = winston.createLogger({
    transports: isBrowser()
        ? [new BrowserConsole()]
        : [new winston.transports.Console()],
    format: winston.format.combine(winston.format.colorize(), winston.format.simple()),
});
