// Core
import chalk from 'chalk';
import { ApolloError } from '@apollo/client';
import { GetServerSidePropsContext } from 'next';
import { parseCookies } from 'nookies';

// type
import { $QueryOptions } from 'types/gql';

// Instruments
import { developmentLogger, productionLogger } from '.';
import { verification } from '../verification';

export const gqlLogger = {
    get verification() {
        return verification.environment();
    },
    info(
        query: $QueryOptions,
        { isStarted, isFinished }: { isStarted?: boolean, isFinished?: boolean },
    ) {
        const graphQlDocument = query.query;
        const currentGraphQLDocument = graphQlDocument.definitions[ 0 ];
        const operation
        = 'operation' in currentGraphQLDocument
            ? currentGraphQLDocument.operation
            : 'unknown graphQl operation';
        const document
        = 'name' in currentGraphQLDocument
            ? currentGraphQLDocument.name && currentGraphQLDocument.name.value
            : 'unknown graphQl document';
        const prefix
        // eslint-disable-next-line no-mixed-operators
        = isStarted && 'was started.' || isFinished && 'was finished.';
        const action = JSON.stringify({ document, operation });
        const message = `GraphQL ${operation} ${action} ${prefix}`;

        developmentLogger.info('INFO', chalk.blueBright(message));
    },
    error(
        query: $QueryOptions,
        error: ApolloError | any,
        context: GetServerSidePropsContext,
    ) {
        const { isDevelopment } = this.verification;
        const graphQlDocument = query.query;
        const currentGraphQLDocument = graphQlDocument.definitions[ 0 ];
        const operation
        = 'operation' in currentGraphQLDocument
            ? currentGraphQLDocument.operation
            : 'unknown graphQl operation';
        const document
        = 'name' in currentGraphQLDocument
            ? currentGraphQLDocument.name && currentGraphQLDocument.name.value
            : 'unknown graphQl document';
        const message = `GraphQL SSR ${operation}`;

        const responseCode
        = error.networkError && 'statusCode' in error.networkError && error.networkError.statusCode;

        const responseMessage
        = error.networkError && 'result' in error.networkError && error.networkError.result.message;

        const networkErrorType = error.networkError && 'Network Error';
        const graphQlErrorType
        = Array.isArray(error.graphQLErrors) && error.graphQLErrors.length > 0 && 'GraphQL Error';
        const errorType = networkErrorType || graphQlErrorType;

        const loggerData = new Map();

        loggerData.set('level', 'error');
        loggerData.set('type', errorType);
        loggerData.set('document', document);
        loggerData.set('operation', operation);

        if (responseCode) {
            loggerData.set('responseCode', responseCode);
        }

        if (responseMessage) {
            loggerData.set('responseMessage', responseMessage);
        }

        if (isDevelopment) {
            loggerData.set('message', chalk.red(message));

            // @ts-ignore
            const transformedLoggerData = Object.fromEntries(loggerData);

            developmentLogger.log(transformedLoggerData);
        } else {
            const fullMessage
            = error instanceof Error
                ? error.message
                : 'unfortunately we don\'t have a message for this error';
            const stack = error instanceof Error && error.stack;

            loggerData.set('name', `GraphQL SSR ${operation} ${errorType}`);
            loggerData.set('message', chalk.red(fullMessage));
            loggerData.set('producer', 'SSR Server');

            if (stack) {
                loggerData.set('stack', stack);
            }

            if (graphQlErrorType) {
                const graphQlQuery
                = query.query && query?.query?.loc?.source && query.query.loc.source.body;
                const graphQLValidationsErrors
                = error.graphQLErrors
                && error.graphQLErrors.length > 0
                && JSON.stringify(error.graphQLErrors);

                if (graphQlQuery) {
                    loggerData.set('body', graphQlQuery);
                }

                if (graphQLValidationsErrors) {
                    loggerData.set('graphQLValidationsErrors', graphQLValidationsErrors);
                }

                // @ts-ignore
                const transformedLoggerData = Object.fromEntries(loggerData);

                productionLogger.log(transformedLoggerData);
            } else if (networkErrorType && context) {
                const cfRequestId = context?.req?.headers[ 'cf-request-id' ];
                const variables = JSON.stringify(query.variables);

                const cookies = parseCookies(context);
                const JWT = cookies?.token;

                if (variables) {
                    loggerData.set('variables', variables);
                }

                if (cfRequestId) {
                    loggerData.set('cfRequestId', cfRequestId);
                }

                if (JWT) {
                    loggerData.set('JWT', JWT);
                }

                const transformedLoggerData = Object.fromEntries(loggerData);

                productionLogger.log(transformedLoggerData);
            }
        }
    },
};
