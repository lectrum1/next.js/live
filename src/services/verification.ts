export const verification = {
    environment: () => {
        const isDevelopment = process.env.NODE_ENV === 'development';
        const isProduction = process.env.NODE_ENV === 'production';

        return {
            isDevelopment,
            isProduction,
        };
    },
    isBrowser: () => typeof window !== 'undefined',
};
