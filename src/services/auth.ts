// Core
import nookies from 'nookies';

// Types
import { GetServerSidePropsContext } from 'next';
import { Auth } from 'gql/queries/user/__generated__/Auth';

// Instruments
import AuthQuery from '../gql/queries/user/auth.graphql';
import { initializeApollo } from '../init/apollo';

export const auth = async (context: GetServerSidePropsContext): Promise<string | null> => {
    const cookies = nookies.get(context);

    if (!cookies?.token) {
        return null;
    }

    try {
        const apolloClient = initializeApollo({}, context);
        const { data, loading, error } = await apolloClient.query<Auth>({
            query:   AuthQuery,
            context: {
                headers: {
                    authorization: cookies?.token ? `Bearer ${cookies?.token}` : '',
                },
            },
        });

        return !error && !loading && data?.auth ? cookies.token : null;
    } catch (error) {
        return null;
    }
};
