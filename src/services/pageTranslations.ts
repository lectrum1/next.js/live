// Core
import { GetServerSidePropsContext, GetStaticPropsContext } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

export const sst = (
    context: GetServerSidePropsContext | GetStaticPropsContext,
    namespaces: string[] = [],
    common = true,
    ...rest: unknown[]
) => {
    const namespacesRequired = common ? ['common'] : [];

    return serverSideTranslations(
        context.locale || 'en',
        namespacesRequired.concat(namespaces),
        ...rest as any,
    );
};
