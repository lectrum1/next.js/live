/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: AddCourseView
// ====================================================

export interface AddCourseView_addView_data_info {
  __typename: "Info";
  requirements: string[];
  descriptions: string[];
  benefits: string[];
  descriptionSummary: string;
}

export interface AddCourseView_addView_data {
  __typename: "Course";
  hash: string;
  badge: boolean;
  rating: number;
  votes: number;
  poster: string;
  duration: number;
  views: number;
  description: string;
  technologies: string;
  createdBy: string;
  price: number;
  created: any;
  info: AddCourseView_addView_data_info;
}

export interface AddCourseView_addView {
  __typename: "CourseDetailReturn";
  data: AddCourseView_addView_data;
}

export interface AddCourseView {
  addView: AddCourseView_addView;
}

export interface AddCourseViewVariables {
  addViewCourseHash: string;
}
