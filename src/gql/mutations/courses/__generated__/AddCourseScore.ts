/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Score } from "./../../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: AddCourseScore
// ====================================================

export interface AddCourseScore_addScore_data_info {
  __typename: "Info";
  requirements: string[];
  descriptions: string[];
  benefits: string[];
  descriptionSummary: string;
}

export interface AddCourseScore_addScore_data {
  __typename: "Course";
  hash: string;
  badge: boolean;
  rating: number;
  votes: number;
  poster: string;
  duration: number;
  views: number;
  description: string;
  technologies: string;
  createdBy: string;
  price: number;
  created: any;
  info: AddCourseScore_addScore_data_info;
}

export interface AddCourseScore_addScore {
  __typename: "CourseDetailReturn";
  data: AddCourseScore_addScore_data;
}

export interface AddCourseScore {
  addScore: AddCourseScore_addScore;
}

export interface AddCourseScoreVariables {
  addScoreCourseHash: string;
  addScoreScore: Score;
}
