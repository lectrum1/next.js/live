/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: MetaCourse
// ====================================================

export interface MetaCourse {
  __typename: "Meta";
  totalDocs: number;
  limit: number;
  page: number | null;
  totalPages: number;
  nextPage: number | null;
  prevPage: number | null;
  pagingCounter: number;
  hasPrevPage: boolean | null;
  hasNextPage: boolean | null;
}
