/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: BaseCourse
// ====================================================

export interface BaseCourse {
  __typename: "Course";
  hash: string;
  badge: boolean;
  rating: number;
  votes: number;
  poster: string;
  duration: number;
  views: number;
  description: string;
  technologies: string;
  createdBy: string;
  price: number;
  created: any;
}
