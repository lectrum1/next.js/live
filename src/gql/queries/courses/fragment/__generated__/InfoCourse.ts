/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: InfoCourse
// ====================================================

export interface InfoCourse {
  __typename: "Info";
  requirements: string[];
  descriptions: string[];
  benefits: string[];
  descriptionSummary: string;
}
