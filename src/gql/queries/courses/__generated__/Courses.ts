/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Courses
// ====================================================

export interface Courses_getAllCourses_data {
  __typename: "Course";
  hash: string;
  badge: boolean;
  rating: number;
  votes: number;
  poster: string;
  duration: number;
  views: number;
  description: string;
  technologies: string;
  createdBy: string;
  price: number;
  created: any;
}

export interface Courses_getAllCourses {
  __typename: "CourseReturn";
  data: Courses_getAllCourses_data[];
}

export interface Courses {
  getAllCourses: Courses_getAllCourses;
}

export interface CoursesVariables {
  page: number;
  limit: number;
}
