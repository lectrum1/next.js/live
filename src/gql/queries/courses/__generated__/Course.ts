/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Course
// ====================================================

export interface Course_getCourseDetails_data_info {
  __typename: "Info";
  requirements: string[];
  descriptions: string[];
  benefits: string[];
  descriptionSummary: string;
}

export interface Course_getCourseDetails_data {
  __typename: "Course";
  hash: string;
  badge: boolean;
  rating: number;
  votes: number;
  poster: string;
  duration: number;
  views: number;
  description: string;
  technologies: string;
  createdBy: string;
  price: number;
  created: any;
  info: Course_getCourseDetails_data_info;
}

export interface Course_getCourseDetails {
  __typename: "CourseDetailReturn";
  data: Course_getCourseDetails_data;
}

export interface Course {
  getCourseDetails: Course_getCourseDetails;
}

export interface CourseVariables {
  getCourseDetailsCourseHash: string;
}
