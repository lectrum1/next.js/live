/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TeacherCourses
// ====================================================

export interface TeacherCourses_getAllTeacherCourses_data_info {
  __typename: "Info";
  requirements: string[];
  descriptions: string[];
  benefits: string[];
  descriptionSummary: string;
}

export interface TeacherCourses_getAllTeacherCourses_data {
  __typename: "Course";
  hash: string;
  badge: boolean;
  rating: number;
  votes: number;
  poster: string;
  duration: number;
  views: number;
  description: string;
  technologies: string;
  createdBy: string;
  price: number;
  created: any;
  info: TeacherCourses_getAllTeacherCourses_data_info;
}

export interface TeacherCourses_getAllTeacherCourses_meta {
  __typename: "Meta";
  totalDocs: number;
  limit: number;
  page: number | null;
  totalPages: number;
  nextPage: number | null;
  prevPage: number | null;
  pagingCounter: number;
  hasPrevPage: boolean | null;
  hasNextPage: boolean | null;
}

export interface TeacherCourses_getAllTeacherCourses {
  __typename: "CourseReturn";
  data: TeacherCourses_getAllTeacherCourses_data[];
  meta: TeacherCourses_getAllTeacherCourses_meta;
}

export interface TeacherCourses {
  getAllTeacherCourses: TeacherCourses_getAllTeacherCourses;
}

export interface TeacherCoursesVariables {
  page: number;
  limit: number;
}
