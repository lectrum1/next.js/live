/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: BaseProfile
// ====================================================

export interface BaseProfile {
  __typename: "Profile";
  name: string;
  email: string;
}
