/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Profile
// ====================================================

export interface Profile_profile {
  __typename: "Profile";
  name: string;
  email: string;
}

export interface Profile {
  profile: Profile_profile;
}
