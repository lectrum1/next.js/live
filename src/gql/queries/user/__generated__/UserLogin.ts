/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Credentials } from '../../../../../__generated__/globalTypes';

// ====================================================
// GraphQL query operation: UserLogin
// ====================================================

export interface UserLogin_login {
  __typename: "Token";
  data: string;
}

export interface UserLogin {
  login: UserLogin_login;
}

export interface UserLoginVariables {
  loginCredentials: Credentials;
}
