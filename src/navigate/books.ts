export const book = Object.freeze({
    home:     '/',
    login:    '/login',
    register: '/register',
    profile:  '/teacher/about',
});
