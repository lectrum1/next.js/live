// Core
import * as Yup from 'yup';

export const validationSchemaSignUp = Yup.object().shape({
    name:  Yup.string().required('Full name is required'),
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),
    password: Yup.string()
        .min(8, 'Password must be at least 8 characters')
        .required('Password is required'),
    confirmPassword: Yup.string()
        .oneOf([Yup.ref('password'), null], 'Passwords must match'),
    // .required('Confirm Password is required'),
});
