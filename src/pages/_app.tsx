// Core
import { AppProps } from 'next/app';
import Head from 'next/head';
import { appWithTranslation } from 'next-i18next';
import { ApolloProvider } from '@apollo/client';
import { enableStaticRendering } from 'mobx-react-lite';
import { configure } from 'mobx';

// Styles
import 'styles/style.css';
import 'styles/bootstrap.min.css';

// Instruments
import { useApollo } from 'lib/hook/useApollo';
import { useStore } from 'lib/hook';
import { StoreProvider } from 'init/store';
import nextI18nConfig from '../../next-i18next.config';
import { verification } from 'services';

const { isBrowser } = verification;

enableStaticRendering(!isBrowser());
configure({
    enforceActions:             'always',
    computedRequiresReaction:   isBrowser(),
    observableRequiresReaction: isBrowser(),
    reactionRequiresObservable: isBrowser(),
});

const CourseApp = ({ Component, pageProps }: AppProps) => {
    const client = useApollo(pageProps.initialApolloState);
    const store = useStore(pageProps.hydrationStore);

    return (
        <>
            <Head>
                <title>Courses page</title>
                <meta charSet = 'utf-8'/>
                <meta httpEquiv = 'X-UA-Compatible' content = 'IE=edge'/>
                <meta name = 'viewport' content = 'width=device-width, shrink-to-fit=9'/>
                <meta name = 'description' content = 'Lectrum LLC'/>
                <meta name = 'author' content = 'Lectrum LLC'/>
                {/* eslint-disable-next-line @next/next/no-page-custom-font */}
                <link
                    rel = 'stylesheet'
                    href = 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap'
                />
            </Head>
            <ApolloProvider client = { client }>
                <StoreProvider dataStore = { store }>
                    <Component { ...pageProps } />
                </StoreProvider>
            </ApolloProvider>
        </>
    );
};

export default appWithTranslation(CourseApp, nextI18nConfig);
