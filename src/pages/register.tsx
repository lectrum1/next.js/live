// Core
import React, { useEffect } from 'react';
import { useMutation } from '@apollo/client';
import { GetServerSidePropsContext, NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';

// Components
import { SignLogo } from 'components/element/auth/Logo';
import { SignFooter } from 'components/element/auth/SignFooter';
import { SignUpForm } from 'components/element/auth/form/SignUpForm';

// gql
import RegisterMutation from '../gql/mutations/user/register.graphql';

// Types
import { Register, RegisterVariables } from 'gql/mutations/user/__generated__/Register';

// Instruments
import { book } from 'navigate/books';
import { auth, sst } from 'services';
import { redirectKey } from 'lib/redirect';
import { useSession } from 'lib/hook';
import { User } from '../../__generated__/globalTypes';

const RegisterPage: NextPage = () => {
    const { t } = useTranslation();
    const { startSession } = useSession();
    const [
        registration,
        {
            data: {
                // @ts-ignore
                register: { data: token },
            } = { register: { data: '' }},
            error,
        },
    ] = useMutation<Register, RegisterVariables>(RegisterMutation);

    useEffect(() => {
        if (token) {
            void startSession(token);
        }
    }, [token]);

    const onSubmit = async (dataForm: User) => {
        await registration({ variables: { registerUser: dataForm }});
    };

    return (
        <div className = 'sign_in_up_bg'>
            <Head>
                <title>{ t('register:title') }</title>
            </Head>
            <div className = 'container'>
                <div className = 'row justify-content-lg-center justify-content-md-center'>
                    <div className = 'col-lg-12'>
                        <SignLogo />
                    </div>
                    <div className = 'col-lg-6 col-md-8'>
                        <div className = 'sign_form'>
                            <h2>{ t('register:h2') }</h2>
                            <p>{ t('register:subtitle') }</p>
                            <SignUpForm
                                onSubmit = { onSubmit }
                                error = { error }
                            />
                            <p className = 'mb-0 mt-30'>
                                { t('register:have-account') } <Link href = { book.login }>
                                    <a>{ t('register:log-in') }</a>
                                </Link>
                            </p>
                        </div>
                        <SignFooter text = { t('footer:copyright', { interpolation: { escapeValue: false }}) }/>
                    </div>
                </div>
            </div>
        </div>
    );
};

export const getServerSideProps = async (context: GetServerSidePropsContext) => {
    if (await auth(context)) {
        return redirectKey(book.home);
    }

    return {
        props: {
            ...await sst(context, ['register', 'footer']),
        },
    };
};

export default RegisterPage;
