// Core
import { GetStaticPropsContext, NextPage } from 'next';
import React from 'react';
import { useTranslation } from 'next-i18next';

// Components
import { ClientError } from 'components/view/error/ClientError';
import { sst } from 'services';

const PageNotFound:NextPage = () => {
    const { t } = useTranslation();

    return (
        <ClientError
            code = { 404 }
            message = { t('404-text') }
        />
    );
};

export const getStaticProps = async (context: GetStaticPropsContext) => {
    return {
        props: {
            ...await sst(context, ['footer']),
        },
    };
};

export default PageNotFound;
