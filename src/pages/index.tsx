// Core
import React from 'react';
import { GetServerSidePropsContext, NextPage } from 'next';
import { getSnapshot } from 'mobx-state-tree';

// Instruments
import { AppLayout } from 'layout/App';
import { Tiles } from 'containers/Tiles';
import { sst, executeQuery } from 'services';
import { useProfile } from 'lib/hook';
import { initializeStore } from 'init/store';

// Components
import { ProfileCard } from 'components/view/Profile/Card';
import { Courses } from 'components/view/Courses';

// gql
import fetchCourses from '../gql/queries/courses/courses.graphql';

const Home: NextPage = () => {
    const { profile: profilePage } = useProfile();

    const tilesProps = {
        tiles:     <Courses/>,
        sideBlock: profilePage ? <ProfileCard profile = { profilePage }/> : null,
    };

    return (
        <AppLayout>
            <Tiles { ...tilesProps }/>
        </AppLayout>
    );
};

export default Home;

export const getServerSideProps = async (context: GetServerSidePropsContext)/*: GetServerSidePropsResult<any>*/ => {
    const {
        page = 1,
        limit = 5,
    } = context.query;

    try {
        const store = initializeStore();
        let courses;

        const initialApolloState = await executeQuery(context, async (execute) => {
            const { data } = await execute({
                query:     fetchCourses,
                variables: { page, limit },
            });

            courses = data?.getAllCourses?.data;
        });
        store.setCourses(courses);

        return {
            props: {
                ...await sst(context, ['menu', 'profile', 'footer', 'course']),
                initialApolloState,
                hydrationStore: getSnapshot(store),
            },
        };
    } catch (error) {
        return { props: {}};
    }
};
