// Core
import React, { useEffect } from 'react';
import { useLazyQuery } from '@apollo/client';
import { GetServerSidePropsContext, NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';

// Components
import { SignLogo } from 'components/element/auth/Logo';
import { SignFooter } from 'components/element/auth/SignFooter';
import { SignInForm } from 'components/element/auth/form/SignInForm';

// Instruments
import { book } from 'navigate/books';
import { auth, sst } from 'services';
import { redirectKey } from 'lib/redirect';
import { useSession } from '../lib/hook';

// gql
import LoginQuery from '../gql/queries/user/login.graphql';

// Types
import { UserLogin, UserLoginVariables } from '../gql/queries/user/__generated__/UserLogin';
import { Credentials } from '../../__generated__/globalTypes';

const LoginPage: NextPage = () => {
    const { startSession } = useSession();
    const { t } = useTranslation();

    const [
        login,
        {
            data: { login: { data: token }} = { login: { data: null  }},
            error,
        },
    ] = useLazyQuery<UserLogin, UserLoginVariables>(LoginQuery);

    useEffect(() => {
        if (token) {
            void startSession(token);
        }
    }, [token]);

    const onSubmit = async (dataForm: Credentials) => {
        await login({ variables: { loginCredentials: dataForm }});
    };

    return (
        <div className = 'sign_in_up_bg'>
            <Head>
                <title>{ t('login:title') }</title>
            </Head>
            <div className = 'container'>
                <div className = 'row justify-content-lg-center justify-content-md-center'>
                    <div className = 'col-lg-12'>
                        <SignLogo />
                    </div>
                    <div className = 'col-lg-6 col-md-8'>
                        <div className = 'sign_form'>
                            <h2>{ t('login:h2') }</h2>
                            <p>{ t('login:subtitle') }</p>
                            <SignInForm
                                onSubmit = { onSubmit }
                                error = { error }
                            />
                            <p className = 'mb-0 mt-30 hvsng145'>
                                { t('login:not-account') } <Link href = { book.register }>
                                    <a>{ t('button-sign-up') }</a>
                                </Link>
                            </p>
                        </div>
                        <SignFooter text = { t('footer:copyright', { interpolation: { escapeValue: false }}) } />
                    </div>
                </div>
            </div>
        </div>
    );
};

export const getServerSideProps = async (context: GetServerSidePropsContext) => {
    if (await auth(context)) {
        return redirectKey(book.home);
    }

    return {
        props: {
            ...await sst(context, ['login', 'footer']),
        },
    };
};

export default LoginPage;
