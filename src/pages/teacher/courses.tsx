// Core
import React from 'react';
import { GetServerSidePropsContext, NextPage } from 'next';
import Head from 'next/head';
import { useTranslation } from 'next-i18next';

// Types
import { TeacherCourses_getAllTeacherCourses_data } from 'gql/queries/courses/__generated__/TeacherCourses';

// Instruments
import { ProfileShort } from 'components/view/Profile/Short';
import { TabNav, TabContent } from 'components/view/Profile/tab';
import { Courses } from 'components/view/Courses';
import { Container } from 'components/element/Container';
import { ProfileLayout } from 'layout/Profile';
import { book } from 'navigate/books';
import { sst, auth } from 'services';
import { redirectKey } from 'lib/redirect';
import { useTeacherCourses } from 'lib/hook';

// teacher/[tab].js enum: about, courses
const TabCourses: NextPage = () => {
    const { t } = useTranslation();
    const { courses = []} = useTeacherCourses();

    const ids = courses.map((crs: TeacherCourses_getAllTeacherCourses_data) => crs.hash);

    return (
        <ProfileLayout>
            { (profile) => (
                <>
                    <Head>
                        <title>{ t('profile:title-courses') }</title>
                    </Head>
                    <ProfileShort { ...profile } />
                    <div className = '_215b15'>
                        <Container>
                            <TabNav
                                activeTab = 'courses'
                                list = { [
                                    { navLabel: t('profile:tab-about'), path: 'about' },
                                    { navLabel: t('profile:tab-courses'), path: 'courses' },
                                ] }
                            />
                        </Container>
                    </div>
                    <div className = '_215b17'>
                        <Container>
                            <TabContent
                                path = 'courses'
                                title = { `My courses (${ids.length})` }
                            >
                                <Courses ids = { ids } />
                            </TabContent>
                        </Container>
                    </div>
                </>
            ) }
        </ProfileLayout>
    );
};

export default TabCourses;

export const getServerSideProps = async (context: GetServerSidePropsContext) => {
    try {
        const token = await auth(context);

        if (!token) {
            return redirectKey(book.login);
        }

        return {
            props: {
                ...await sst(context, ['profile', 'menu', 'footer']),
            },
        };
    } catch (error) {
        return { props: {}};
    }
};
