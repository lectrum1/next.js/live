// Core
import React from 'react';
import { GetServerSidePropsContext, NextPage } from 'next';
import Head from 'next/head';
import { useTranslation } from 'next-i18next';

// Instruments
import { ProfileShort } from 'components/view/Profile/Short';
import { TabNav, TabContent } from 'components/view/Profile/tab';
import { Container } from 'components/element/Container';
import { ProfileLayout } from 'layout/Profile';
import { auth } from 'services/auth';
import { book } from 'navigate/books';
import { sst } from 'services';
import { redirectKey } from 'lib/redirect';

const TabAbout: NextPage = () => {
    const { t } = useTranslation();

    return (
        <ProfileLayout>
            { (profile) => (
                <>
                    <Head>
                        <title>{ t('profile:title-about') }</title>
                    </Head>
                    <ProfileShort { ...profile } />
                    <div className = '_215b15'>
                        <Container>
                            <TabNav
                                activeTab = 'about'
                                list = { [
                                    { navLabel: t('profile:tab-about'), path: 'about' },
                                    { navLabel: t('profile:tab-courses'), path: 'courses' },
                                ] }
                            />
                        </Container>
                    </div>
                    <div className = '_215b17'>
                        <Container>
                            <TabContent
                                path =  'about'
                                title = 'About Me'
                            >
                                {t('profile:text-about')}
                            </TabContent>
                        </Container>
                    </div>
                </>
            ) }
        </ProfileLayout>
    );
};

export const getServerSideProps = async (context: GetServerSidePropsContext) => {
    if (!await auth(context)) {
        return redirectKey(book.login);
    }

    return { props: { ...await sst(context, ['profile', 'menu', 'footer']) }};
};

export default TabAbout;
