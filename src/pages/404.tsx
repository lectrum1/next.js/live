// Core
import { GetStaticPropsContext, NextPage } from 'next';
import React from 'react';

// Components
import { ClientError } from 'components/view/error/ClientError';
import { sst } from 'services';

const PageServerError:NextPage = () => {
    return (
        <ClientError
            code = { 500 }
            message = { 'Internal Server Error' }
        />
    );
};

export const getStaticProps = async (context: GetStaticPropsContext) => {
    return {
        props: {
            ...await sst(context, ['footer']),
        },
    };
};

export default PageServerError;
