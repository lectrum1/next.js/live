// Core
import { GetServerSidePropsContext } from 'next';
import React, { FC } from 'react';

// Components
import { CourseLayout } from 'layout/Course';
import { Container } from 'components/element/Container';
import { Rating } from 'components/view/Courses/Rating';
import { CourseDetailInfo } from 'components/view/Courses/DetailInfo';
import { executeQuery, sst } from 'services';

// gql
import fetchCourse from '../../gql/queries/courses/course.graphql';

const Course: FC = () => {
    return (
        <CourseLayout>
            {// @ts-ignore
                (course = {}) => (
                    <>
                        <div className = '_215b15 _byt1458'>
                            <Container>
                                <Rating/>
                            </Container>
                        </div>
                        <div className = '_215b17'>
                            <Container>
                                <CourseDetailInfo { ...course }/>
                            </Container>
                        </div>
                    </>
                )}
        </CourseLayout>
    );
};

export default Course;

export const getServerSideProps = async (context: GetServerSidePropsContext) => {
    try {
        const { hash } = context.query;
        const initialApolloState = await executeQuery(context, async (execute) => {
            await execute({
                query:     fetchCourse,
                variables: { getCourseDetailsCourseHash: hash },
            });
        });

        return {
            props: {
                ...await sst(context, ['profile', 'menu', 'footer']),
                initialApolloState,
            },
        };
    } catch (error) {
        return { props: {}};
    }
};
