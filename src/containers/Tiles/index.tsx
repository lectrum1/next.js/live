// Core
import React, { FC, ReactElement, ReactNode } from 'react';

interface IProps {
    tiles: ReactNode,
    sideBlock: ReactElement | null
}

export const Tiles: FC<IProps> = ({
    tiles,
    sideBlock,
}) => {
    const authorize = Boolean(sideBlock);

    return (
        <div className = 'container-fluid'>
            <div className = 'row'>
                <div className = { `col-xl-${authorize ? '9' : '12'} col-lg-${authorize ? '8' : '12'}` }>
                    { tiles }
                </div>
                { authorize && (
                    <div className = 'col-xl-3 col-lg-4'>
                        <div className = 'right_side'>
                            { sideBlock }
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
};
