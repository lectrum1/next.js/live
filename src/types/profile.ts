export type Path = 'about' | 'courses';

export type DataRender = any;

//@todo replace to Profile graphql
export interface IProfile {
    // token:      string | null,
    name:       string,
    email?:     string,
    password?:  string,
}
