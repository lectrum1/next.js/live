// Core
import type { Instance } from 'mobx-state-tree';

// Model
import { RootStore } from 'models';

export type IStore = Instance<typeof RootStore>;
