import type { Instance } from 'mobx-state-tree';
import { Course } from 'models/course';

export interface ICourse extends Instance<typeof Course> {}

// type CourseInfo = {
//     requirements: string[],
//     descriptions: string[];
//     benefits: string[];
//     descriptionSummary: string;
// }

// export interface ICourse {
//     hash: string;
//     poster: string;
//     badge?: boolean;
//     rating?: number;
//     votes?: number;
//     duration?: number;
//     views?: number;
//     description?: string;
//     technologies?: string;
//     createdBy?: string;
//     price?: number;
//     info?: CourseInfo;
//     created?: Date;
// }
