// Core
import { GetServerSidePropsContext, NextPageContext } from 'next';

export type ContextInto = NextPageContext | GetServerSidePropsContext;
