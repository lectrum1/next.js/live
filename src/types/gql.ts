// Types
import type { ApolloQueryResult } from '@apollo/client/core/types';
import type { QueryOptions } from '@apollo/client/core/watchQueryOptions';

export type $QueryOptions = QueryOptions<unknown>;

export interface IExecutor<TResult extends ApolloQueryResult<any>> {
    (execute: (options: $QueryOptions) => Promise<TResult | undefined>): Promise<TResult | undefined>
}
