// Some code for
// - error indicated
// - response status indicated
// - interface throw categorize
// - other

export enum StatusCode {
    OK = 200,
    OKNoContent = 204,
    BadRequest = 400,
    Unauthorized = 401,
    Forbidden = 403,
    NotFound = 404,
    TooManyRequests = 429,
}
