// Core
import React, { FC, ReactNode, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useMutation } from '@apollo/client';
import Head from 'next/head';

// Components
import { Footer } from 'components/view/Footer';
import { Menu } from 'components/view/Menu';
import { CoursePreview } from 'components/view/Courses/Preview';

// Types
import { ICourse } from 'types';
import { AddCourseView, AddCourseViewVariables } from 'gql/mutations/courses/__generated__/AddCourseView';

// Instruments
import { useCourse } from 'lib/hook';
import { developmentLogger } from 'services';

// gql
import addViewMutate from '../../gql/mutations/courses/addView.graphql';

type CourseChild = (variable?: ICourse) => ReactNode;

interface IPropsCourseLayout {
    HeaderSection?: FC;
    FooterSection?: FC;
    children: CourseChild
}

export const CourseLayout: FC<IPropsCourseLayout> = ({
    children,
    HeaderSection = Menu,
    FooterSection = Footer,
}) => {
    const { query } = useRouter();
    const {
        data: {
            getCourseDetails: { data: course },
        } = { getCourseDetails: {}},
        loading,
    } = useCourse(query?.hash as string);

    const [addView] = useMutation<AddCourseView, AddCourseViewVariables>(addViewMutate);

    useEffect(() => {
        if (course && !loading && query?.hash) {
            addView({ variables: { addViewCourseHash: query?.hash as string }})
                .catch((errorMutate) => developmentLogger.error('Error', errorMutate));
        }
    }, [course, loading]);

    return (
        <>
            <Head>
                <title>Course hash: { course?.hash }</title>
            </Head>
            <HeaderSection />
            <div className = 'wrapper _bg4586'>
                { course && (<CoursePreview { ...course } />) }
                { children(course) }
                <FooterSection />
            </div>
        </>
    );
};
