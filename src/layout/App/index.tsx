// Core
import React, { FC, ReactElement } from 'react';

// Components
import { Footer } from 'components/view/Footer';
import { Menu } from 'components/view/Menu';

interface IProps {
    HeaderSection?: FC,
    FooterSection?: FC,
}

export const AppLayout: FC<IProps> = ({
    children,
    HeaderSection = Menu,
    FooterSection = Footer,
    ...rest
}) => (
    <>
        <HeaderSection />
        <div className = 'wrapper'>
            <div className = 'sa4d25'>
                { React.cloneElement(children as ReactElement, rest) }
                <FooterSection />
            </div>
        </div>
    </>
);
