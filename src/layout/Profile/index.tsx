// Core
import React, { FC, ReactNode } from 'react';

// Types
import { Profile_profile, Profile } from '../../gql/queries/user/__generated__/Profile';

// Instruments
import { Footer } from 'components/view/Footer';
import { Menu } from 'components/view/Menu';
import { useProfile } from 'lib/hook';

type ProfileChild = (variable: Profile_profile) => ReactNode;

interface IProps {
    HeaderSection?: FC;
    FooterSection?: FC;
    children: ReactNode | ProfileChild
}

export const ProfileLayout: FC<IProps> = ({
    children, HeaderSection = Menu, FooterSection = Footer,
}) => {
    // @ts-ignore
    const { profile } = useProfile<Profile>();

    return (
        <>
            <HeaderSection />
            <div className = 'wrapper _bg4586'>
                { typeof children === 'function'
                /* some data fetching next provide to layout children */
                    ? children(profile)
                    : children }
                <FooterSection />
            </div>
        </>
    );
};
