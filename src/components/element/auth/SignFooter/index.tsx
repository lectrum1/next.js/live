// Core
import { FC } from 'react';

export const SignFooter: FC<{ text: string }> = ({ text = '' }) => (
    <div className = 'sign_footer'>
        { text }
    </div>
);
