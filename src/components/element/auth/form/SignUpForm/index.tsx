// Types
import type { FieldErrors } from 'react-hook-form';
import { ApolloError } from '@apollo/client';

// Core
import { BaseSyntheticEvent, FC } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useTranslation } from 'next-i18next';

// Schema
import { validationSchemaSignUp } from 'shapes/signUp';

import { User } from '../../../../../../__generated__/globalTypes';

type DataForm = Record<keyof User | 'confirmPassword', string>;
// User & { confirmPassword: string }; // ?

interface IPropsSignUp {
    onSubmit: (data: DataForm, event?: BaseSyntheticEvent) => Promise<void> | void,
    onError?: (error: { [key in keyof User]?: FieldErrors }, event?: BaseSyntheticEvent) => Promise<void>,
    error?: ApolloError,
}

export const SignUpForm: FC<IPropsSignUp> = ({
    onSubmit, onError, error,
}) => {
    const { t } = useTranslation();
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<DataForm>({
        resolver: yupResolver(validationSchemaSignUp) as any,
    });

    return (
        <form onSubmit = {
            handleSubmit(
                (data, event) => {
                    reset();
                    void onSubmit(data, event);
                },
                onError,
            ) }
        >
            <div className = 'ui search focus'>
                <div className = 'ui left icon input swdh11 swdh19'>
                    <input
                        className = 'prompt srch_explore'
                        id = 'id_fullname'
                        type = 'text'
                        placeholder = { t('register:field-name-ph') }
                        defaultValue = ''
                        { ...register('name', {
                            required:  true,
                            maxLength: 64,
                        }) }
                    />
                    <p className = 'invalid-feedback d-block'>{errors.name?.message}</p>
                </div>
            </div>
            <div className = 'ui search focus mt-15'>
                <div className = 'ui left icon input swdh11 swdh19'>
                    <input
                        className = 'prompt srch_explore'
                        id = 'id_email'
                        type = 'email'
                        defaultValue = { '' }
                        placeholder = { t('register:field-email-ph') }
                        { ...register('email', {
                            required:  true,
                            maxLength: 64,
                        }) }
                    />
                    <p className = 'invalid-feedback d-block'>{errors.email?.message}</p>
                </div>
            </div>
            <div className = 'ui search focus mt-15'>
                <div className = 'ui left icon input swdh11 swdh19'>
                    <input
                        className = 'prompt srch_explore'
                        id = 'id_password'
                        type = 'password'
                        defaultValue = ''
                        placeholder = { t('register:field-pass-ph') }
                        { ...register('password', {
                            required:  true,
                            minLength: 8,
                            maxLength: 64,
                        }) }
                    />
                    <p className = 'invalid-feedback d-block'>{errors.password?.message}</p>
                </div>
            </div>
            <div className = 'ui search focus mt-15'>
                <div className = 'ui left icon input swdh11 swdh19'>
                    <input
                        className = 'prompt srch_explore'
                        type = 'password'
                        defaultValue = ''
                        placeholder = { t('register:field-repass-ph') }
                        { ...register('confirmPassword', {
                            required:  true,
                            minLength: 8,
                            maxLength: 64,
                        }) }
                    />
                    <p className = 'invalid-feedback d-block'>{errors.confirmPassword?.message}</p>
                </div>
            </div>
            <p className = 'invalid-feedback d-block'>{error?.message}</p>
            <button className = 'login-btn' type = 'submit'>{ t('button-sign-up') }</button>
        </form>
    );
};
