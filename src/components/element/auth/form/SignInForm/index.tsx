// Types
import type { FieldErrors } from 'react-hook-form';
import { ApolloError } from '@apollo/client';

// Core
import { BaseSyntheticEvent, FC } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useTranslation } from 'next-i18next';

import { Credentials } from '../../../../../../__generated__/globalTypes';

// Schema
import { validationSchemaSignIn } from 'shapes/signIn';

interface IPropsSignIn {
    onSubmit: (data: Credentials, event?: BaseSyntheticEvent) => Promise<void> | void,
    onError?: (errors: { [key in keyof Credentials]?: FieldErrors }, event?: BaseSyntheticEvent) => Promise<void>,
    error?: ApolloError,
}

export const SignInForm: FC<IPropsSignIn> = ({
    onSubmit,
    onError,
    error,
}) => {
    const { register, handleSubmit, formState: { errors }} = useForm<Credentials>({
        resolver: yupResolver(validationSchemaSignIn) as any,
    });
    const { t } = useTranslation();

    return (
        <form onSubmit = { handleSubmit(onSubmit, onError) }>
            <div className = 'ui search focus mt-15'>
                <div className = 'ui left icon input swdh95'>
                    <input
                        className = 'prompt srch_explore'
                        id = 'id_email'
                        type = 'email'
                        defaultValue = { '' }
                        placeholder = { t('login:field-email-ph') }
                        { ...register('email', {
                            required:  true,
                            maxLength: 64,
                        }) }
                    />
                    <p className = 'invalid-feedback d-block'>{errors.email?.message}</p>
                </div>
            </div>
            <div className = 'ui search focus mt-15'>
                <div className = 'ui left icon input swdh95'>
                    <input
                        className = 'prompt srch_explore'
                        id = 'id_password'
                        type = 'password'
                        defaultValue = ''
                        placeholder = { t('login:field-pass-ph') }
                        { ...register('password', {
                            required:  true,
                            maxLength: 64,
                        }) }
                    />
                    <p className = 'invalid-feedback d-block'>{errors.password?.message}</p>
                </div>
            </div>
            <div className = 'ui form mt-30 checkbox_sign'>
                <div className = 'inline field'>
                    <div className = 'ui checkbox mncheck'>
                        <input
                            type = 'checkbox'
                            tabIndex = { 0 }
                            className = 'hidden'
                            id = 'remember'
                        />
                        <label htmlFor = 'remember'>{ t('login:label-remember-me') }</label>
                    </div>
                </div>
            </div>
            <p className = 'invalid-feedback d-block'>{error?.message}</p>
            <button className = 'login-btn' type = 'submit'>{t('button-sign-in')}</button>
        </form>
    );
};
