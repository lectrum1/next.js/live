// Core
import Link from 'next/link';
import Image from 'next/image';

// Image
import logoImg from '../../../../../public/images/logo.svg';

export const SignLogo = () => (
    <div className = 'main_logo25' id = 'logo'>
        <Link href = '/' passHref>
            <a>
                <Image
                    src = { logoImg }
                    alt = 'to home'
                    layout = 'fixed'
                    width = { 135 }
                    height = { 32 }
                />
            </a>
        </Link>
    </div>
);
