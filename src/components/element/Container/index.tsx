// Core
import React, { FC } from 'react';

interface IProperties {
    fluid?: boolean;
    size?: boolean;
    column?: number;
    rowClass?: string
}

export const Container: FC<IProperties> = ({
    children,
    fluid = true,
    size = 'lg',
    column = 12,
    rowClass = '',
}) => (
    <div className = { `container${fluid ? '-fluid' : ''}` }>
        <div className = { `row ${rowClass}` }>
            <div className = { `col-${size}-${column}` }>{ children }</div>
        </div>
    </div>
);
