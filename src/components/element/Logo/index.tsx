// Core
import React, { FC } from 'react';
import Image from 'next/image';
import Link from 'next/link';

interface IProps {
    children?: never
}

export const Logo: FC<IProps> = () => (
    <div className = 'main_logo' id = 'logo'>
        <Link href = '/'>
            <a>
                <Image
                    src = '/images/logo.svg'
                    alt = ''
                    layout = 'fill'
                />
            </a>
        </Link>
        { /* <Link href = '/'>
            <a>
                <Image
                    className = 'logo-inverse'
                    src = '/images/ct_logo.svg'
                    alt = 'logo'
                    width = { width }
                    height = { height }
                />
            </a>
        </Link> */ }
    </div>
);
