// Core
import React, { FC, ReactNode } from 'react';
import { useForm } from 'react-hook-form';

type Inputs = {
    example: string,
    exampleRequired: string,
};

interface IProps {
    children: ReactNode,
    onSubmit: (data: unknown) => Promise<void>
}

export const Form: FC<IProps> = ({
    children,
    onSubmit,
}) => {
    const { handleSubmit } = useForm<Inputs>();

    return (
        <form onSubmit = { handleSubmit(onSubmit) }>
            {children}
        </form>
    );
};
