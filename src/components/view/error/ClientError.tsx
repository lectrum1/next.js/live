// Core
import React, { FC } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';

// Types
import { StatusCode } from 'types';

import { book } from 'navigate/books';

interface IPropsError {
    code: StatusCode,
    message: string,
    info?: string,
}

export const ClientError: FC<IPropsError> = ({ code, message, info }) => {
    const { t } = useTranslation();

    return (
        <body className = 'coming_soon_style'>
            <div className = 'wrapper coming_soon_style'>
                <div className = 'container'>
                    <div className = 'row'>
                        <div className = 'col-md-12'>
                            <div className = 'cmtk_group'>
                                <div className = 'ct-logo'>
                                    <Link href = '/'>
                                        <a>
                                            <Image
                                                src = '/images/ct_logo.svg'
                                                alt = 'logo'
                                                width = { 150 }
                                                height = { 36 }
                                            />
                                        </a>
                                    </Link>
                                </div>
                                <div className = 'cmtk_dt'>
                                    <h1 className = 'title_404'>{ code }</h1>
                                    <h4 className = 'thnk_title1'>{ message }</h4>
                                    <span>{ info }</span>
                                    <div>
                                        <Link href = { book.home }>
                                            <a className = 'bk_btn'>{ t('go-to-home') }</a>
                                        </Link>
                                    </div>
                                </div>
                                <div className = 'tc_footer_main'>
                                    <div className = 'tc_footer_left'>
                                        <ul>
                                            <li>
                                                <Link href = { book.home }>
                                                    <a>{ t('home') }</a>
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className = 'tc_footer_right'>
                                        <p>
                                            { t('footer:copyright', { interpolation: { escapeValue: false }}) }
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>
    );
};
