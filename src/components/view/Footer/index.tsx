// Core
import React, { FC } from 'react';
import { useTranslation } from 'next-i18next';

export const Footer: FC = () => {
    const { t } = useTranslation();

    return (
        <footer className = 'footer mt-30'>
            <div className = 'container'>
                <div className = 'row'>
                    <div className = 'col-lg-12'>
                        <div className = 'footer_bottm'>
                            <div className = 'row'>
                                <div className = 'col-md-6'>
                                    <ul className = 'fotb_left'>
                                        <li>
                                            <p>{ t('footer:copyright', { interpolation: { escapeValue: false }}) }</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
};
