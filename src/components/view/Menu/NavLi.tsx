// Core
import React, { FC } from 'react';
import Link, { LinkProps } from 'next/link';

interface IProps {
    liProps?: {className: string};
    linkProps?: LinkProps;
    subLinkProps: { className: string, title: string, onClick?: () => void };
}

export const NavLi: FC<IProps> = ({
    liProps = {},
    linkProps = { href: '#' },
    subLinkProps = { className: 'opts_account', title: 'SubLink title' },
    children = 'NavLink',
}) => (
    <li { ...liProps }>
        <Link { ...linkProps }>
            <a { ...subLinkProps }>
                { children }
            </a>
        </Link>
    </li>
);
