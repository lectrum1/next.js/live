// Core
import React, { FC, useCallback } from 'react';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';

// Instruments
import { NavLi } from './NavLi';
import { Logo } from 'components/element/Logo';
import { book } from 'navigate/books';
import { useProfile, useLiveLogout } from 'lib/hook';

export const Menu: FC = () => {
    const router = useRouter();
    const { pathname, asPath, locale, reload } = router;
    // @ts-ignore
    const { profile } = useProfile();
    const { t } = useTranslation();
    const { mutate: logout } = useLiveLogout();

    const changeLocale = useCallback((localeChanged: string) => () => {
        router
            .push(pathname, asPath, { locale: localeChanged })
            .catch((error) => { throw error; });
    }, [ router, pathname ]);

    const navLang = ['ru', 'en', 'de']
        .filter((lang) => lang !== locale)
        .map((lang, idx) => (
            <button
                key = { `${idx + lang}` }
                className = 'lang-box'
                onClick = { changeLocale(lang) }
            >
                { lang.toUpperCase() }
            </button>
        ));

    return (
        <header className = 'header clearfix'>
            <Logo />
            <div className = 'header_right'>
                <ul>
                    <li> { navLang } </li>
                    <NavLi
                        subLinkProps = { {
                            className: 'upload_btn',
                            title:     'Create New Course',
                        } }
                    >{ t('menu:create-course') }</NavLi>
                    <NavLi
                        liProps      = { { className: 'ui dropdown' } }
                        linkProps = { { href: book.profile } }
                        subLinkProps = { {
                            className: 'opts_account _df7852',
                            title:     'Account',
                        } }
                    >
                        {profile?.name || t('profile:default-user-name')}
                    &nbsp;
                        <Image
                            src = '/images/hd_dp.jpg'
                            alt = 'logo'
                            layout = 'intrinsic'
                            width = { 40 }
                            height = { 40 }
                        />
                    </NavLi>
                    { profile && (
                        <NavLi
                            liProps      = { { className: 'ui dropdown' } }
                            subLinkProps = { {
                                className: 'opts_account log_out _5f7g11',
                                title:     'Log out',
                                onClick:   () => {
                                    logout();
                                    reload();
                                },
                            } }
                        >
                            { t('menu:logout') }
                        </NavLi>
                    ) }
                </ul>
            </div>
        </header>
    );
};
