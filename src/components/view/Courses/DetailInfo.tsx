// Core
import { FC, Fragment } from 'react';

// types
import { ICourse } from 'types';

const Items = ({
    list = [],
    as: As = Fragment,
}: {
    list?: string[],
    as?: FC | string,
}) => (
    <ul className = '_abc124'>
        {
            list.map((child, idx) => (
                <li key = { String(idx) }>
                    <span className = '_5f7g11'>
                        <As>{ child }</As>
                    </span>
                </li>
            ))
        }
    </ul>
);

export const CourseDetailInfo: FC<ICourse> = (course) => {
    const requirements = <Items list = { course.info?.requirements }/>;
    const descriptions = <Items list = { course.info?.descriptions }/>;
    const technologies = <Items list = { course.info?.benefits } as = 'strong' />;

    return (
        <div className = 'course_tab_content'>
            <div className = 'tab-content' id = 'nav-tabContent'>
                <div
                    className = 'tab-pane fade show active'
                    id = 'nav-about'
                    role = 'tabpanel'
                >
                    <div className = '_htg451'>
                        <div className = '_htg452'>
                            <h3>Requirements</h3>
                            { requirements }
                        </div>
                        <div className = '_htg452 mt-35'>
                            <h3>Description</h3>
                            <ul className = '_abc124'>
                                { descriptions }
                            </ul>
                            <p>
                                { course.info?.descriptionSummary }
                            </p>
                            <p>
                                Throughout the course we cover tons of tools and
                                technologies including:
                            </p>
                            <ul className = '_abc124'>
                                { technologies }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
