// Core
import React, { FC } from 'react';
import { observer } from 'mobx-react-lite';

// Instruments
import { Course } from './Course';
import { useSelect } from 'lib/hook';

// Types
import { DataRender, ICourse } from 'types';

interface IPropsCourses {
    ids?: DataRender
}

export const Courses: FC<IPropsCourses> = observer(({ ids = []}) => {
    const courses = useSelect((store) => store.courses);
    const list = courses?.filter((crs: ICourse) => !ids.length || ids.includes(crs.hash));

    return (
        <div className = 'section3125'>
            <div className = 'la5lo1'>
                <div className = 'featured_courses'>
                    { list?.map((course: ICourse) => (<Course key = { course.hash } { ...course } />)) }
                </div>
            </div>
        </div>
    );
});
