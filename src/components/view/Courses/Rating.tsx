// Core
import React, { FC } from 'react';
import Image from 'next/image';

// Hook
import { useProfile } from 'lib/hook';

interface IPropsRating {
    likes?: number;
    dislikes?: number;
}

export const Rating: FC<IPropsRating> = ({
    likes = 20,
    dislikes = 100,
}) => {
    // @ts-ignore
    const { profile } = useProfile();

    return (
        <div className = 'user_dt5'>
            <div className = 'user_dt_left'>
                <div className = 'live_user_dt'>
                    <div className = 'user_img5'>
                        <Image
                            src = '/images/hd_dp.jpg'
                            alt = ''
                            layout = 'fixed'
                            height = { 50 }
                            width = { 50 }
                        />
                    </div>
                    <div className = 'user_cntnt'>
                        <p className = '_df7852'>{profile?.name || 'Guest'}</p>
                    </div>
                </div>
            </div>
            <div className = 'user_dt_right'>
                <ul>
                    <li>
                        <div className = 'lkcm152'>
                            <Image
                                src = '/images/like.svg'
                                className = 'like-icon'
                                alt = ''
                                layout = 'fixed'
                                width = { 24 }
                                height = { 24 }
                            />
                            <span>{likes}</span>
                        </div>
                    </li>
                    <li>
                        <div className = 'lkcm152'>
                            <Image
                                src = '/images/dislike.svg'
                                className = 'like-icon'
                                alt = ''
                                layout = 'fixed'
                                width = { 24 }
                                height = { 24 }
                            />
                            <span>{dislikes}</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    );
};
