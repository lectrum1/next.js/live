// Core
import React, { FC } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';

// Types
import { ICourse } from 'types';

export const Course: FC<ICourse> = (props) => {
    const { t } = useTranslation();
    const { locale } = useRouter();

    const {
        hash,
        badge,
        rating,
        poster: posterImage,
        duration,
        views,
        description,
        technologies,
        createdBy,
        price,
    } = props;

    return (
        <div className = 'item'>
            <div className = 'fcrse_1 mb-20'>
                <Link href = { `/courses/${hash}` } locale = { locale }>
                    <a className = 'fcrse_img'>
                        <Image
                            src = { posterImage }
                            layout = 'fill'
                            objectFit = 'cover'
                            objectPosition = 'center'
                            alt = { 'description' }
                        />
                        <div className = 'course-overlay'>
                            <div className = 'badge_seller'>{ badge }</div>
                            <div className = 'crse_reviews'>
                                <i className = 'uil uil-star' />
                                { rating }
                            </div>
                            <div className = 'crse_timer'>
                                { duration }
                                { ' ' }
                                { t('course:hour', { count: duration }) }
                            </div>
                        </div>
                    </a>
                </Link>
                <div className = 'fcrse_content'>
                    <div className = 'vdtodt'>
                        <span className = 'vdt14'>
                            { views }
                            { ' ' }
                            { t('course:view', { count: views }) }
                        </span>
                        <span className = 'vdt14'>
                            { t('course:about') }
                            { ' ' }
                            { duration }
                            { ' ' }
                            { t('course:days-ago') }
                        </span>
                    </div>
                    <Link href = { `/courses/${hash}` } locale = { locale }>
                        <a className = 'crse14s'>
                            { description }
                        </a>
                    </Link>
                    <Link href = '#'>
                        <a className = 'crse-cate'>
                            { technologies }
                        </a>
                    </Link>
                    <div className = 'auth1lnkprce'>
                        <p className = 'cr1fot'>
                            By
                            { ' ' }
                            <Link href = '#'>
                                <a>{ createdBy }</a>
                            </Link>
                        </p>
                        <div className = 'prce142'>
                            { price }
                            $
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
