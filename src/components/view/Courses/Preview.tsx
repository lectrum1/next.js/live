// Core
import React, { FC } from 'react';
import Image from 'next/image';

// Instruments
import { Container } from 'components/element/Container';

// Types
import { ICourse } from 'types';

export const CoursePreview: FC<ICourse> = ({
    poster: posterImage,
    description,
    rating,
    votes,
    created = new Date(),
}) => (
    <div className = '_216b01'>
        <Container>
            <div className = 'section3125'>
                <div className = 'row justify-content-center'>
                    <div className = 'col-xl-4 col-lg-5 col-md-6'>
                        <div className = 'preview_video'>
                            <a
                                href = '#'
                                className = 'fcrse_img'
                                data-toggle = 'modal'
                                data-target = '#videoModal'
                            >
                                <Image
                                    src = { posterImage }
                                    layout = 'fill'
                                    objectFit = 'cover'
                                    alt = { 'description' }
                                />
                                <div className = 'course-overlay'>
                                    <div className = 'badge_seller'>Bestseller</div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div className = 'col-xl-8 col-lg-7 col-md-6'>
                        <div className = '_215b03'>
                            <h2>S O M E  T I T L E</h2>
                            <span className = '_215b04'>{ description }</span>
                        </div>
                        <div className = '_215b05'>
                            <div className = 'crse_reviews mr-2'>
                                <i className = 'uil uil-star'/>{ rating }
                            </div>
                            ({ votes } ratings)
                        </div>
                        <div className = '_215b05'>000,000 students enrolled</div>
                        <div className = '_215b06'>
                            <div className = '_215b07'>
                                <span><i className = 'uil uil-comment'/></span>
                                s o m e  l a n g u a g e
                            </div>
                        </div>
                        <div className = '_215b05'>Last updated { new Date(created).toLocaleDateString('en-US') }</div>
                    </div>
                </div>
            </div>
        </Container>
    </div>
);
