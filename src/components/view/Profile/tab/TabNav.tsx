// Core
import React, { FC } from 'react';
import Link from 'next/link';

// Types
import { Path } from 'types';

interface IPropsTabNav {
    activeTab: string;
    list: TabNavType[]
}

type TabNavType = {
    navLabel: string,
    path: Path
}

export const TabNav: FC<IPropsTabNav> = ({ activeTab, list }) => (
    <div className = 'course_tabs'>
        <nav>
            <div
                className = 'nav nav-tabs tab_crse'
                id = 'nav-tab'
                role = 'tablist'
            >
                { list.map((tab) => (
                    <Link key = { tab.path } href = { `/teacher/${tab.path}` }>
                        <a
                            className = { `nav-item nav-link${tab.path === activeTab ? ' active' : ''}` }
                            id = { `nav-${tab.path}-tab` }
                            data-toggle = 'tab'
                            role = 'tab'
                            aria-selected = { tab.path === activeTab }
                        >
                            { tab.navLabel }
                        </a>
                    </Link>
                )) }
            </div>
        </nav>
    </div>
);
