// Core
import React, { FC } from 'react';

interface IPropTabContent {
    title: string,
    path: string,
}
export const TabContent: FC<IPropTabContent> = ({
    title, path, children,
}) => {
    return (
        <div className = 'course_tab_content'>
            <div className = 'tab-content' id = 'nav-tabContent'>
                <div
                    className = 'tab-pane fade show active'
                    id = { `nav-${path}` }
                    role = 'tabpanel'
                >
                    <h3> { title } </h3>
                    { children }
                </div>
            </div>
        </div>
    );
};

