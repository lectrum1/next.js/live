// Core
import React, { FC } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';

// Constants
import { book } from 'navigate/books';

// Types
import { IProfile } from 'types';

interface IPropsProfileCard {
    profile: IProfile
}

export const ProfileCard: FC<IPropsProfileCard> = ({ profile }) => {
    const { t } = useTranslation();

    return profile ? (
        <div className = 'fcrse_2 mb-30'>
            <div className = 'tutor_img'>
                <Link href = { book.profile }>
                    <a>
                        <Image
                            src = '/images/hd_dp.jpg'
                            alt = ''
                            width = { 96 }
                            height = { 96 }
                        />
                    </a>
                </Link>
            </div>
            <div className = 'tutor_content_dt'>
                <div className = 'tutor150'>
                    <Link href = { book.profile }>
                        <a className = 'tutor_name'>{ profile.name }</a>
                    </Link>
                    <div className = 'mef78' title = 'Verify'>
                        <i className = 'uil uil-check-circle' />
                    </div>
                </div>
                <div className = 'tutor_cate'>
                    Web Developer, Designer, and Teacher
                </div>
                <div className = 'tut1250'>
                    <span className = 'vdt15'>
                        112
                        { ' ' }
                        { t('profile:students') }
                    </span>
                    <span className = 'vdt15'>
                        3
                        { ' ' }
                        { t('profile:courses') }
                    </span>
                </div>
                <Link href = { book.profile }>
                    <a className = 'prfle12link'>{t('go-to-profile')}</a>
                </Link>
            </div>
        </div>
    ) : null;
};
