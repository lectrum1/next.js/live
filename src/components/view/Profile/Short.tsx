// Core
import React, { FC } from 'react';
import Image from 'next/image';

// Types
import { IProfile } from 'types';

export const ProfileShort: FC<IProfile> = ({ name, email }) => (
    <div className = '_216b01'>
        <div className = 'container-fluid'>
            <div className = 'row justify-content-md-center'>
                <div className = 'col-md-10'>
                    <div className = 'section3125 rpt145'>
                        <div className = 'row'>
                            <div className = 'col-lg-7'>
                                <div className = 'dp_dt150'>
                                    <div className = 'img148'>
                                        <Image
                                            src = '/images/hd_dp.jpg'
                                            alt = ''
                                            width = { 106 }
                                            height = { 106 }
                                        />
                                    </div>
                                    <div className = 'prfledt1'>
                                        <h2>{ name }</h2>
                                        <span>{ '<' }{ email }{ '>  ' }</span>
                                        <span>Web Developer, Designer, and Teacher</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);
