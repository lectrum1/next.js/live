// Core
import { useQuery } from '@apollo/client';

// Types
import { Profile } from '../../gql/queries/user/__generated__/Profile';

// GQL
import GetProfileQuery from '../../gql/queries/user/profile.graphql';

export const useProfile = <T = Profile>() => {
    const { data, loading, error } = useQuery<T>(GetProfileQuery);

    return {
        // @ts-ignore
        profile: data?.profile,
        loading,
        error,
    };
};
