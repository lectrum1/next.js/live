// Core
import { useContext } from 'react';

// Other
import { StoreContext } from 'init/store';
import { IStore } from 'types';

export function useSelect<
    TState = IStore,
    Selected = unknown
>(selector: (state: TState) => Selected): Selected {
    const context = useContext(StoreContext);

    if (context === undefined) {
        throw new Error('useRootStore must be used within RootStoreProvider');
    }

    // @ts-ignore
    return selector(context);
}
