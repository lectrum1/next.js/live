// Core
import { useMemo } from 'react';

// Init
import { initializeApollo } from 'init/apollo';

export const useApollo = (initialState = {}) => {
    return useMemo(() => initializeApollo(initialState), [initialState]);
};
