// Core
import { useQuery } from '@apollo/client';
import nookies from 'nookies';

// gql
import authQuery from '../../gql/queries/user/auth.graphql';

// Types
import { Auth } from 'gql/queries/user/__generated__/Auth';

export const useAuth = () => {
    const cookies = nookies.get();

    const { data: { auth } = { auth: false }, loading, error } = useQuery<Auth>(authQuery);

    return {
        token: cookies?.token,
        auth,
        loading,
        error,
    };
};
