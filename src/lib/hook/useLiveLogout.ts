// Core
import { useLazyQuery } from '@apollo/client';

// gql
import logoutQuery from '../../gql/queries/user/logout.graphql';

// types
import { UserLogout } from 'gql/queries/user/__generated__/UserLogout';

// Instruments
import { destroySession } from './useSession';

export const useLiveLogout = () => {
    const [ logout ] = useLazyQuery<UserLogout>(logoutQuery);

    return {
        mutate() {
            logout();
            destroySession();
        },
    };
};
