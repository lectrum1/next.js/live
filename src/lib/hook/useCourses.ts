// Core
import { useQuery } from '@apollo/client';

// Query
import getAllCourses from '../../gql/queries/courses/courses.graphql';

// Types
import { Courses, CoursesVariables } from 'gql/queries/courses/__generated__/Courses';

type PropsType = {
    page?: number;
    limit?: number;
}

export const useCourses = ({ page = 1, limit = 20 }: PropsType) => {
    const { data, error, loading } = useQuery<Courses, CoursesVariables>(getAllCourses, {
        variables: {
            page,
            limit,
        },
    });

    return {
        loading,
        error,
        data: data?.getAllCourses.data,
    };
};
