// Core
import { useMemo } from 'react';

import { initializeStore } from 'init/store';
import { IStore } from 'types';

export function useStore(initialState: IStore) {
    return useMemo(
        () => initializeStore(initialState),
        [initialState],
    );
}
