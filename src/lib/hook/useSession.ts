// Core
import { destroyCookie, setCookie } from 'nookies';
import { useRouter } from 'next/router';

// Instruments
import { book } from 'navigate/books';

export function destroySession() {
    destroyCookie(null, 'token');
}

export const useSession = (redirectPath = book.home) => {
    const { replace } = useRouter();

    return {
        startSession(token: string) {
            setCookie(null, 'token', token, {
                maxAge: 24 * 60 * 60,
                path:   book.home,
            });

            return replace(redirectPath);
        },
        destroySession,
    };
};
