// Core
import { useQuery } from '@apollo/client';

// gql
import TeacherCoursesQuery from '../../gql/queries/courses/teacherCourses.graphql';
import { TeacherCourses } from 'gql/queries/courses/__generated__/TeacherCourses';

export const useTeacherCourses = (page = 1, limit = 20) => {
    const { data, loading, error } = useQuery<TeacherCourses>(TeacherCoursesQuery, {
        variables: { page, limit },
    });

    return {
        courses: data?.getAllTeacherCourses.data,
        loading,
        error,
    };
};

