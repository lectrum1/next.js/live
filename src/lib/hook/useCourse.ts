// Core
import { useQuery } from '@apollo/client';

// gql
import fetchCourse from '../../gql/queries/courses/course.graphql';

// types
import { CourseVariables } from 'gql/queries/courses/__generated__/Course';
import { ICourse } from 'types';

type Course = {
    getCourseDetails: {
        data: ICourse
    }
}
export const useCourse = (hash: string) => {
    return useQuery<Course, CourseVariables>(fetchCourse, {
        variables: { getCourseDetailsCourseHash: hash },
        skip:      hash === undefined,
    });
};

