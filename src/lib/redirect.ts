export const redirectKey = (destination: string, permanent = false) => ({
    redirect: { destination, permanent },
});
