// Core
import TransportStream from 'winston-transport';

export default class BrowserConsole extends TransportStream {
    constructor(options = {}) {
        super(options);

        this.setMaxListeners(30);
    }

    log(info: any, next: () => void) {
        // eslint-disable-next-line
        console.log(
            `[dev-${info.level}]:`,
            info.message,
        );

        next();
    }
}
