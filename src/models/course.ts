// Core
import { types } from 'mobx-state-tree';

const CourseInfo = types.model({
    __typename:         types.optional(types.string, 'CourseInfo'),
    requirements:       types.optional(types.array(types.string), []),
    descriptions:       types.optional(types.array(types.string), []),
    benefits:           types.optional(types.array(types.string), []),
    descriptionSummary: types.optional(types.string, ''),
});

export const Course = types
    .model({
        __typename:   types.optional(types.string, 'Course'),
        hash:         types.string,
        poster:       types.string,
        badge:        types.optional(types.boolean, false),
        rating:       types.optional(types.number, 0),
        votes:        types.optional(types.number, 0),
        duration:     types.optional(types.number, 0),
        views:        types.optional(types.number, 0),
        description:  types.optional(types.string, ''),
        technologies: types.optional(types.string, ''),
        createdBy:    types.optional(types.string, ''),
        price:        types.optional(types.number, 0),
        created:      types.optional(types.string, ''),
        info:         types.optional(types.compose(CourseInfo, CourseInfo), {}),
    });
