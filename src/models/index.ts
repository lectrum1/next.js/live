// Core
import { types } from 'mobx-state-tree';

// Models
import { Course } from './course';

// Types
import { ICourse } from 'types';

export const RootStore = types
    .model({
        courses: types.array(Course),
    })
    .actions((self) => ({
        setCourses(courses?: ICourse[]) {
            // @ts-ignore
            self.courses = courses;
        },
    }));
