module.exports = {
    client: {
        includes: ['src/**/*'],
        tagName: "graphql",
        addTypename: false,
    },
};
