/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export interface CourseInput {
  badge?: boolean | null;
  poster?: string | null;
  duration: number;
  description: string;
  technologies: string;
  price: number;
  info: InfoInput;
}

export interface Credentials {
  email: string;
  password: string;
}

export interface InfoInput {
  requirements: string[];
  descriptions: string[];
  benefits: string[];
  descriptionSummary: string;
}

export interface Score {
  score: number;
}

export interface User {
  name: string;
  email: string;
  password: string;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
