const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CleanCss = require('clean-css');
const { i18n } = require('./next-i18next.config');

module.exports = {
    images: {
        domains: ['lectrum.io', 'placeimg.com'],
    },
    reactStrictMode: true,
    webpack: (config, { isServer }) => {
        const isProduction = process.env.NODE_ENV === 'production';

        if (isProduction) {
            config.optimization.minimizer.push(
                new OptimizeCssAssetsPlugin({
                    assetNameRegExp: /\.css$/g,
                    cssProcessor: CleanCss,
                    cssProcessorOptions: {
                        level: {
                            1: {
                                all: true,
                                normalizeUrls: false,
                            },
                            2: {
                                restructureRules: true,
                                removeUnusedAtRules: true,
                                skipProperties: ['border-top', 'border-bottom'],
                            },
                        },
                    },
                    canPrint: true,
                })
            );
        }

        if (!isServer) {
            config.resolve.fallback = {
                fs: false,
                os: false,
                path: false,
                zlib: false,
                http: false,
                https: false,
            };

            return {
                ...config,
            };
        }

        return config;
    },
    i18n,
};
