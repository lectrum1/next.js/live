Vitaliy Osadchiy nextjs project
---
> This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

### 🤖 Краткий обзор команд для проекта

> Заметка: запускать через `yarn «имя команды»` или `npm run «имя команды»`.

"dev": "next dev",
"build": "next build",
"start": "next start",
"lint": "next lint"
| Команда           | Описание                                                                        |
| ----------------- | ------------------------------------------------------------------------------- |
| `prebuild`        | хард-удаления папки целевой дистрибутива                                        |
| `dev`             | запустить проект в режиме разработки                                            |
| `build`           | запустить сборку client                                                         |
| `start`           | запустить проект для разработки                                                 |
| `lint`            | провести анализ всего исходного кода на стилистические ошибки                   |
| `lint:fix`        | провести анализ всего исходного кода на стилистические ошибки c авто-фиксом     |

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
